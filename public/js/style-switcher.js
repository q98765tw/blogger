/* toggler style switcher*/
const styleSwitcherToggle = document.querySelector(".style-switcher-toggler");
styleSwitcherToggle.addEventListener("click",() => {
    document.querySelector(".style-switcher").classList.toggle("open");
})
// hidden style-switcher on scroll
window.addEventListener("scroll",()=> {
    if(document.querySelector(".style-switcher").classList.contains("open"))
    {
        document.querySelector(".style-switcher").classList.remove("open")
    }
})
/* li class active */
const liActive=document.querySelectorAll(".li-active")
function setActiveLi(id){
    liActive.forEach((style) =>{
        if(id == style.getAttribute("href")){
            style.classList.add("active")
        }else{
            style.classList.remove("active")
        }
    })
}
/* theme colors*/
const alternateStyles = document.querySelectorAll(".alternate-style");
var color;
function setActiveStyle(color){
    clearInterval(clear);
    alternateStyles.forEach((style) =>{
        if(color === style.getAttribute("title")){
            style.removeAttribute("disabled");
        }else{
            style.setAttribute("disabled","true");
        }
    })
}
var clear = setInterval(x, 5000);
function x(){
    var change = "color-"+Math.floor(Math.random()*5);
    alternateStyles.forEach((style) =>{
        if(change == style.getAttribute("title")){
            style.removeAttribute("disabled");
        }else{
            style.setAttribute("disabled","true");
        }
    })
}
/* theme light and dark mode */
const dayNight = document.querySelector(".day-night");
dayNight.addEventListener("click",()=>{
    dayNight.querySelector("i").classList.toggle("fa-sun")
    dayNight.querySelector("i").classList.toggle("fa-moon")
    document.body.classList.toggle("dark")
})
window.addEventListener("load",()=>{
    if(document.body.classList.contains("dark")){
        dayNight.querySelector("i").classList.add("fa-sun")
    }else{
        dayNight.querySelector("i").classList.add("fa-moon")
    }
})