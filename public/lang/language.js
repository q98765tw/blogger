$.i18n.lang = $.extend(true, $.i18n.lang || {}, {
    "en": {
        home: "Home",
        about: "About",
        portfolio: "Portfolio",
        Contact: "Contact"
    },
    'zh-tw': {
        home: "首頁",
        about: "關於我",
        portfolio: "作品集",
        Contact: "聯絡我"
    }
});