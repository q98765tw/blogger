/* typing animation */
var typed = new Typed(".typing", {
    strings: ["", "Frondend Developer", "Web  Developer"],
    typeSpeed: 100,
    BackSpeed: 60,
    loop: true
})
/* scroll down */
window.addEventListener('scroll', reveal);

function reveal() {
    var reveals = document.querySelectorAll('.reveal');

    for (var i = 0; i < reveals.length; i++) {
        var windowheight = window.innerHeight;
        var revealtop = reveals[i].getBoundingClientRect().top;
        var revealpoint = 150;

        if (revealtop < windowheight - revealpoint) {
            reveals[i].classList.add('active');
        } else {
            reveals[i].classList.remove('active');
        }
    }
}
/* i18n */
$(function () {
    $('#btnTw').click(function () {
        $.i18n.setLocale('zh-tw');
        alert("changeTW")
        location.reload();
    });
    $('#btnEn').click(function () {
        $.i18n.setLocale('en');
        alert("changeEN")
        location.reload();
    });
    $.i18n.load('../public/lang/language.js', function (success) {
        console.log($.i18n.prop('about'))
        if (success) {
            $('#homeList').text($.i18n.prop('home'));
            $('#aboutList').text($.i18n.prop('about'));
            $('#portfolioList').text($.i18n.prop('portfolio'));
            $('#contactList').text($.i18n.prop('Contact'));

        }
    })
})